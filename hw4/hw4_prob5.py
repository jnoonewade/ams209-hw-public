def print_yourName(l,bool):
	first = l[0]
	last = l[1]
	if bool:
		print first.title() + ' ' + last.title()
	else:
		print last.title() + ', ' + first.title()

your_name = ['Mike','is a cool guy']

print_yourName(your_name,False)
