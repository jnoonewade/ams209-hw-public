def is_palindrome(s):
	return s == s[::-1]

your_palindrome = 'I AM A PALINDROMEMORDNILAP A MA I'

if is_palindrome(your_palindrome):
	print 'Yup, its a palindrome'
else:
	print 'Nope, thats not a palindrome'
