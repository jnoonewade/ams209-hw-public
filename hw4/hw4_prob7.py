def is_triangle(a,b,c):
	return a + b > c and b + c > a and a + c > b

your_triangle = [2,2,2.5]
if is_triangle(your_triangle[0],your_triangle[1],your_triangle[2]):
	print 'Yup, those sides make a triangle'
else:
	print 'Nope, those sides do not make a triangle'
