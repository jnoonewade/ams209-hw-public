import os
from numpy import loadtxt
import matplotlib.pyplot as plt

def make_make():
	os.chdir("../newton_rootFinder/")
	os.system("make clean")
	os.system("make")

def runtimeParameters_init(runName,method,xstart,xend,maxiter,thresh,ftn,init,multi):
	os.chdir("../newton_rootFinder/")
	i = 0
	for filename in os.listdir("."):
		if filename.startswith("rootFinder.init"):
			i += 1
	if i != 0:
		os.rename("rootFinder.init","rootFinder.init." + str(i))
	
	params = open("rootFinder.init","w+")
	params.write("run_name         %s \n" % runName)
	params.write("method_type      %s \n" % method)
	params.write("x_beg            %s \n" % xstart)
	params.write("x_end            %s \n" % xend)
	params.write("max_iter         %s \n" % maxiter)
	params.write("threshold        %s \n" % thresh)
	params.write("ftn_type         %s \n" % ftn)
	params.write("init_guess       %s \n" % init)
	params.write("multiplicity     %s \n" % multi)
	params.close()

def run_rootFinder():
	os.system(r'"../newton_rootFinder/rootFinder.exe"')

def plot_data(plt_fileName):
	os.chdir("../newton_rootFinder/")
	iternum = []
	solu = []
	err = []
	with open('rootFinder_newton.dat') as f:
		for line in f:
			pdata = map(float,line.split())
			iternum.append(pdata[0])
			solu.append(pdata[1])
			err.append(pdata[3])

	plt.figure(figsize=(6,6))
	plt.subplot(2,1,1)
	plt.plot(iternum,solu,label = 'Solution')
	plt.xlabel('Iteration Number')
	plt.ylabel('Solution')
	plt.title('Solution vs Iteration Number')
	plt.ylim([1,2])
	plt.legend()

	plt.subplot(2,1,2)
	plt.plot(iternum,err,label = 'Error')
	plt.xlabel('Iteration Number')
	plt.ylabel('Error')
	plt.title('Error vs Iteration Number')
	plt.ylim([0,0.8])
	plt.legend()
	
	plt.tight_layout()
	plt.savefig(plt_fileName)
	plt.show()
	plt.close()
	#rename dat file
	i = 0
        for filename in os.listdir("."):
                if filename.startswith("rootFinder_newton.dat"):
                        i += 1
        if i != 0:
                os.rename("rootFinder_newton.dat","rootFinder_newton.dat." + str(i))

if __name__ == '__main__':
	runName = str('newton')
	method = str('newton')
	xstart = -8.0
	xend =  10.0
	maxiter = 1000
	ftn = 2
	multi = 0
	
	#initial guess = 1.5
	#threshold = 1.e-4
	init = 1.5
	thresh = str(1.e-4)
	make_make()
	runtimeParameters_init(runName,method,xstart,xend,maxiter,thresh,ftn,init,multi)
	run_rootFinder()
	plot_data("ftn_%s_%s_%s.png" % (str(ftn),str(init),str(thresh)) )

	#threshold = 1.e-6
        thresh = str(1.e-6)
        make_make()
        runtimeParameters_init(runName,method,xstart,xend,maxiter,thresh,ftn,init,multi)
        run_rootFinder()
        plot_data("ftn_%s_%s_%s.png" % (str(ftn),str(init),str(thresh)) )

	#threshold = 1.e-8
        thresh = str(1.e-8)
        make_make()
        runtimeParameters_init(runName,method,xstart,xend,maxiter,thresh,ftn,init,multi)
        run_rootFinder()
        plot_data("ftn_%s_%s_%s.png" % (str(ftn),str(init),str(thresh)) )

	#initial guess = 5
        #threshold = 1.e-4
        init = 5 
        thresh = str(1.e-4)
        make_make()
        runtimeParameters_init(runName,method,xstart,xend,maxiter,thresh,ftn,init,multi)
        run_rootFinder()
        plot_data("ftn_%s_%s_%s.png" % (str(ftn),str(init),str(thresh)) )

        #threshold = 1.e-6
        thresh = str(1.e-6)
        make_make()
        runtimeParameters_init(runName,method,xstart,xend,maxiter,thresh,ftn,init,multi)
        run_rootFinder()
        plot_data("ftn_%s_%s_%s.png" % (str(ftn),str(init),str(thresh)) )

        #threshold = 1.e-8
        thresh = str(1.e-8)
        make_make()
        runtimeParameters_init(runName,method,xstart,xend,maxiter,thresh,ftn,init,multi)
        run_rootFinder()
        plot_data("ftn_%s_%s_%s.png" % (str(ftn),str(init),str(thresh)) )
