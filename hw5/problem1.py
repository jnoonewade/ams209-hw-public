#Part (a):

#Store all words
allwords = []
with open('words.txt') as fn:
	for line in fn:
		word = line.strip()
		allwords.append(word)

#Store the first n words
nwords = []
userwords = input('How many words? ')
with open('words.txt') as fn:
	for line_no, line in enumerate(fn):
		word = line.strip()
		nwords.append(word)
		if line_no == userwords:
			break

#Part (b):

#Store all words in dictionary
alldict = {}
i = 0
with open('words.txt') as fn:
	for line in fn:
		word = line.strip()
		alldict[i] = word
		i += 1

#Store the first n words in dictionary
ndict = {}
i = 0
with open('words.txt') as fn:
	for line_no, line in enumerate(fn):
		word = line.strip()
		ndict[i] = word
		i += 1
		if line_no == userwords-1:
			break

#Check if word is in the dictionary
usercheck = raw_input('What string to check is in the %i long dictionary? ' % userwords)
if usercheck in ndict.values():
	print '%s is in there!' % usercheck
else:
	print '%s is not there!' % usercheck
