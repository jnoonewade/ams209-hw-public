factors = {'meter': 1, 'mile': 0.000621, 'inch': 39.370079, 'foot': 3.28084, 'yard': 1.093613}
multiples = { 'nm': (10^9), 'um': (10^6), 'mm': (10^3), 'cm': (10^2), 'km': (10^(-3))}

userunit = 's'
while (not(factors.has_key(userunit))):
	userunit = raw_input('Please input your desired unit system (meter, mile, inch, foot, yard): ')

	usernum = input('Please input the length in that system (number only): ')

	if factors.has_key(userunit):
		for key in factors:
			if key <> userunit:
				fac1 = usernum/factors.get(userunit)*factors.get(key)
				print "%f %s is %f %s" % (usernum, userunit, fac1, key)
		for key in multiples:
			fac2 = factors.get(userunit)*multiples.get(key)
			print "%f %s is %f %s" % (usernum, userunit, fac2, key)
	else:
		print "\nERROR: Please type the unit correctly, it must match one of the 5 listed\n"
