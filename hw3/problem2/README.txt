Responses to some of the exercises:

3. Plot from GNUplot - "function.jpg"

4. Do not need to recompile when chaning
   rootFinder.init - this file gets input
   each time the code is called, so new
   parameter values are updated

5. Added a third function, change ftn_type
   to value 3 to test

6. The code seems to work.. I'm not exactly
   sure what this file does right now..

7. Works normally, even after compiling.

8. I expect nothing to happen as there
   are no errors.
