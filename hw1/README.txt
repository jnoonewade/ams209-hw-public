The commands used to produce the solutions
to homework 1 are in the 'commands_used.txt'.
Some commands will create files, others will
output numbers, depending on the question. 
Problem 7 and 8 commands are to be inserted into the .bashrc
file.

