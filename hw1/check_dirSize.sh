#!/bin/bash 

# check disk usage of local ams209 cloned directory
# and print list to screen
du --max-depth=0 /home/mathlete/ams209-classweb/* | sort -nr

# print the top 3 largest directories or files to output.txt
du --max-depth=0 /home/mathlete/ams209-classweb/* | sort -nr | head -3 > output.txt
