program approx_pi

! This program will approximate the value of pi
! using a truncated sum given in the problem
! discription along with 4 specificed error tolerances
! 
! complie and run the code using the following commands:
! gfortran -fdefault-real-8 approx_pi.f90
! ./a.out

implicit none

real :: pi_approx, pi_true, thresh, pi_diff
integer :: N, eightN

! pi = arccos(-1)
pi_true = acos(-1.d0)

! compute pi using formula and thresh = 1.e-4
N = 0
thresh = 1.e-4
pi_diff = pi_true
pi_approx = 0.

do while (pi_diff > thresh)

eightN = 8*N
pi_approx = pi_approx + 16.**(-N)*( 4./(eightN+1) - 2./(eightN+4) - 1./(eightN+5) - 1./(eightN+6) )

pi_diff = abs(pi_true - pi_approx)
N = N + 1

end do

print *, "Threshold value 1.e-4"
print *, "Number of terms used: ", N
print *, "|pi_approx - pi_true| = ", pi_diff


! compute pi using formula and thresh = 1.e-8
N = 0
thresh = 1.e-8
pi_diff = pi_true
pi_approx = 0.

do while (pi_diff > thresh)

eightN = 8*N
pi_approx = pi_approx + 16.**(-N)*( 4./(eightN+1) - 2./(eightN+4) - 1./(eightN+5) - 1./(eightN+6) )

pi_diff = abs(pi_true - pi_approx)
N = N + 1

end do

print *, "Threshold value 1.e-8"
print *, "Number of terms used: ", N
print *, "|pi_approx - pi_true| = ", pi_diff


! compute pi using formula and thresh = 1.e-12
N = 0
thresh = 1.e-12
pi_diff = pi_true
pi_approx = 0.

do while (pi_diff > thresh)

eightN = 8*N
pi_approx = pi_approx + 16.**(-N)*( 4./(eightN+1) - 2./(eightN+4) - 1./(eightN+5) - 1./(eightN+6) )

pi_diff = abs(pi_true - pi_approx)
N = N + 1

end do

print *, "Threshold value 1.e-12"
print *, "Number of terms used: ", N
print *, "|pi_approx - pi_true| = ", pi_diff


! compute pi using formula and thresh = 1.e-16
N = 0
thresh = 1.e-16
pi_diff = pi_true
pi_approx = 0.

do while (pi_diff > thresh)

eightN = 8*N
pi_approx = pi_approx + 16.**(-N)*( 4./(eightN+1) - 2./(eightN+4) - 1./(eightN+5) - 1./(eightN+6) )

pi_diff = abs(pi_true - pi_approx)
N = N + 1

end do

print *, "Threshold value 1.e-16"
print *, "Number of terms used: ", N
print *, "|pi_approx - pi_true| = ", pi_diff


end program approx_pi
