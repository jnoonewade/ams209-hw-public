

module trap_rule_mod

public f

contains

function trap_func(T,a,b,N)

real, intent(out) :: T
real, intent(in) :: a,b
integer, intent(in) :: N

real :: dx
integer :: i

dx = (b - a)/N 

T = f(a) + f(b)

do i = 1, N-1, 1

	T = T + 2*f(a + dx*i) 

end do

T = dx/2 * T

end function trap_func


subroutine trap_sub(T,a,b,N)

real, intent(out) :: T
real, intent(in) :: a,b
integer, intent(in) :: N

real :: dx
integer :: i

dx = (b - a)/N 

T = f(a) + f(b)

do i = 1, N-1, 1

	T = T + 2*f(a + dx*i) 

end do

T = dx/2 * T


end subroutine trap_sub

real(kind = 8) function f(x)

	f = x**2 + 1

end function

subroutine trap_exact(F,a,b)

real, intent(out) :: F
real, intent(in) :: a, b

	F = b**3/3 + b - (a**3/3 + a)

end subroutine trap_exact

end module trap_rule_mod
