program trap_rule

! This program solves the integral of the
! function f(x) = x^2 + 1 on the interval [0,1]
! using the trapezoidal rule with a secified 
! number of subintervals.
! Compile and run the program using the following
! commands:
! gfortran -fdefault-real-8 -c trap_rule_mod.f90
! gfortran -fdefault-real-8 -c trap_rule.f90
! gfortran trap_rule_mod.o trap_rule.o -o trap_rule.exe
! ./trap_rule.exe


use trap_rule_mod

implicit none

real :: trapFuncVal, trapSubVal, trapExact
real :: a, b, T
integer :: N

a = 0
b = 1

print *, "Input an integer value for the number "
print *, "of subintervals:"
read *, N

trapFuncVal = trap_func(T,a,b,N) 
print *, "[1] Trapezoidal in function   = ", trapFuncVal

call trap_sub(trapSubVal,a,b,N)
print *, "[2] Trapezoidal in subroutine = ", trapSubVal

call trap_exact(trapExact,a,b)
print *, "[3] Exact integration         = ", trapExact

print *, "[4] Error in function         = ", abs(trapFuncVal - trapExact)

print *, "[5] Error in subroutine       = ", abs(trapSubVal - trapExact)

end program trap_rule
